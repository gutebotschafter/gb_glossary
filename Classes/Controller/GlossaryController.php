<?php
namespace GuteBotschafter\GbGlossary\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2009-2015 Gute Botschafter GmbH, Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use GuteBotschafter\GbGlossary\Domain\Model\Definition;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

class GlossaryController extends ActionController
{
    /**
     * @var array $validLetters
     */
    protected $validLetters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    /**
     * @var \GuteBotschafter\GbGlossary\Domain\Repository\DefinitionRepository
     */
    protected $definitionRepository;

    /**
     * injectDefinitionRepository
     *
     * @param \GuteBotschafter\GbGlossary\Domain\Repository\DefinitionRepository $definitionRepository
     */
    public function injectDefinitionRepository(\GuteBotschafter\GbGlossary\Domain\Repository\DefinitionRepository $definitionRepository)
    {
        $this->definitionRepository = $definitionRepository;
    }

    /**
     * List action for this controller. Displays glossary index list
     */
    public function listAction()
    {
        $this->view->assign('currentPage', $GLOBALS['TSFE']->id);
        $this->view->assignMultiple(
            $this->prepareQueryResult($this->definitionRepository->findAll())
        );
    }

    /**
     * Subset action for this controller. Displays a subset of the glossary list
     *
     * @param string $startLetter
     * @return string The rendered view
     */
    public function subsetAction($startLetter = NULL)
    {
        $startLetter = $this->normalizeStartLetter($startLetter);
        $this->view->assignMultiple([
            'currentPage' => $GLOBALS['TSFE']->id,
            'startLetter' => $startLetter
        ]);
        $this->view->assignMultiple(
            $this->prepareQueryResult($this->definitionRepository->findAll(), $startLetter, $forceSubset = TRUE)
        );
    }

    /**
     * Details action for this controller. Displays glossary entry details
     *
     * @param \GuteBotschafter\GbGlossary\Domain\Model\Definition $definition
     * @param string $backPid
     * @param string $startLetter
     */
    public function showAction(Definition $definition, $backPid = NULL, $startLetter = NULL)
    {
        $this->view->assignMultiple([
            'definition' => $definition,
            'backPid' => $backPid,
            'startLetter' => $startLetter
        ]);
    }

    /**
     * Retrieve and prepare the glossary items for display
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryResultInterface $entries
     * @param string $startLetter
     * @param bool $forceSubset
     * @return array
     */
    protected function prepareQueryResult(QueryResultInterface $entries, $startLetter = NULL, $forceSubset = FALSE)
    {
        $definitions = $letters = [];
        foreach ($entries as $entry) {
            $letter = $this->normalizeStartLetter($entry->getShort());
            $definitions[$letter][$entry->getShort()] = $entry;
        }
        foreach (array_merge($this->validLetters, ['?']) as $letter) {
            $letters[$letter] = array_key_exists($letter, $definitions);
        }
        if (!empty($startLetter)) {
            $definitions = [$startLetter => $definitions[$startLetter]];
        } elseif ($forceSubset) {
            $definitions = NULL;
        }

        return [
            'definitions' => $definitions,
            'letters' => $letters
        ];
    }

    /**
     * Normalize/extract a startLetter to conform to the expected display chars
     *
     * @param string $startLetter
     * @return string
     */
    protected function normalizeStartLetter($startLetter)
    {
        $normalizedLetter = strtoupper(substr(iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $startLetter), 0, 1));
        // If we don't know the letter assign it to the generic ? symbol
        if (!empty($startLetter) && !in_array($normalizedLetter, $this->validLetters)) {
            $normalizedLetter = '?';
        }
        return $normalizedLetter;
    }
}
