<?php
namespace GuteBotschafter\GbGlossary\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2009-2015 Gute Botschafter GmbH, Dominik Hahn <d.hahn@gute-botschafter.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class EvalStringLength
{
    /**
     * Custom javascript validator that limits string lenght to 255 chars
     *
     * @return string
     */
    public function returnFieldJS()
    {
        return 'return value.substring(0, 253);';
    }

    /**
     * TCE helper function to limit string length to 255 chars
     *
     * @param string $value
     * @param string $is_in
     * @param integer $set
     * @return string
     */
    public function evaluateFieldValue($value, $is_in, &$set)
    {
        return substr($value, 0, 255);
    }
}
