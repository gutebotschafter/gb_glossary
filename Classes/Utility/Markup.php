<?php
namespace GuteBotschafter\GbGlossary\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2009-2015 Gute Botschafter Gmbh, Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class Markup
{
    /**
     * TYPO3 cObject
     *
     * @var object
     */
    public $cObj = NULL;

    /**
     * Word Replacement List
     *
     * @var array
     */
    protected $glossaryEntries = array();

    /**
     * Word Replacement List Index
     *
     * @var array
     */
    protected $glossaryIndex = array();

    /**
     * Configuration Information
     *
     * @param array
     */
    protected $config = NULL;

    /**
     * HTML body dom node
     *
     * @var \DOMNode
     */
    protected $body = NULL;

    /**
     * @var \Masterminds\HTML5
     */
    protected $html5 = NULL;

    /**
     * Constructor for User_Tx_GbGlossary_Markup
     *
     * @access public
     * @throws \Exception
     */
    public function __construct()
    {
        $this->cObj = GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
        $this->html5 = GeneralUtility::makeInstance('Masterminds\\HTML5');
    }

    /**
     * public function to mark the glossary entries in the html fragment
     *
     * @param string $content
     * @param array $config
     * @return string
     */
    public function markGlossaryWords($content = '', $config = array())
    {
        $this->config = $config;

        // Skip excludes pages
        if (GeneralUtility::inList($this->config['excludePages'], $GLOBALS['TSFE']->id) || $GLOBALS['TSFE']->id == $this->config['detailsPid']) {
            return $content;
        }
        // retrieve the glossary words from database
        $this->getGlossaryWords();

        $this->parseHTMLContent($content);
        $this->insertTooltipPlaceholders($this->body);
        return $this->createToolTips();
    }

    /**
     * Recursively walk the dom tree, skipping defined tags and working only on nodes of TEXT type
     *
     * @param \DOMNode &$node
     */
    protected function insertTooltipPlaceholders(\DOMNode &$node)
    {
        // make sure we are always working with lowercase tagname
        $this->config['excludeTags'] = strtolower($this->config['excludeTags']);
        if ($node->nodeName != 'a' && !GeneralUtility::inList($this->config['excludeTags'], $node->nodeName)) {
            if ($node->nodeType == XML_TEXT_NODE && strlen(trim($node->nodeValue)) >= 1) {
                $this->markToolTip($node);
            } else {
                $cNodes = $node->childNodes;
                if ($cNodes->length > 0) {
                    foreach ($cNodes as $cNode) {
                        $this->insertTooltipPlaceholders($cNode);
                    }
                }
            }
        }
    }

    /**
     * Prepare regular expressions for all glossary words and call function to mark as glossary word on matches
     *
     * @access protected
     * @param \DOMNode &$node XML_TEXT_NODE to work on
     * @return void
     */
    protected function markToolTip(&$node)
    {
        $searchWords = $replaceWords = array();
        foreach (array_keys($this->glossaryEntries) as $glossaryWord) {
            $regexModifier = ($this->config['ignoreCase'] == TRUE ? 'i' : '');
            $searchWords[] = '#([.,:_\s=-]|\A)(' . preg_quote($glossaryWord) . ')([.,:_\s=-]|\Z)#' . $regexModifier;
        }
        $node->nodeValue = preg_replace_callback($searchWords, array($this, 'markGlossaryWord'), $node->nodeValue);
    }

    /**
     * Convert the \DOMDocument back into an HTML string and transform all markers
     *
     * @return string
     */
    protected function createToolTips()
    {
        return preg_replace_callback('/%%%(\d+)\|([^%]*)%%%/', array($this, 'wrapGlossaryWord'), $this->html5->saveHTML($this->body));
    }

    /**
     * Transform the markers into real HTML code since we have finished parsing the DOM
     *
     * @access protected
     * @param array $matches array of matched elements in the subject
     * @return string
     */
    protected function wrapGlossaryWord($matches)
    {
        $uid = $matches[1];
        $source = $matches[2];
        $definition = $this->glossaryEntries[$this->glossaryIndex[$uid]];
        $linkParams = array(
            'additionalParams' => '&tx_gbglossary_main[controller]=Glossary&tx_gbglossary_main[action]=show&tx_gbglossary_main[definition]=' . intval($uid),
            'ATagParams' => 'class="gbglossary_csstooltip"',
            'useCacheHash' => 1
        );
        if (MathUtility::canBeInterpretedAsInteger($this->config['detailsPid']) && (int)$this->config['detailsPid']) {
            $linkParams['parameter'] = intval($this->config['detailsPid']);
        } else {
            $linkParams['parameter'] = $GLOBALS['TSFE']->id;
            $linkParams['additionalParams'] = '';
        }

        return $this->cObj->typolink('<span class="dfn">' . $definition['longversion'] . '<br/><strong>' . LocalizationUtility::translate('hint', 'gb_glossary') . '</strong></span>' . $source, $linkParams);
    }

    /**
     * Mark all glossary entries in the XML_TEXT_NODE without creating HTML code which gets escaped by DOM functions
     *
     * @access protected
     * @param array $matches array of matched elements in the subject
     * @return string
     */
    protected function markGlossaryWord($matches)
    {
        $index = ($this->config['ignoreCase'] == TRUE ? strtolower($matches[2]) : $matches[2]);
        $definition = $this->glossaryEntries[$index];
        return $matches[1] . '%%%' . $definition['uid'] . '|' . $matches[2] . '%%%' . $matches[3];
    }

    /**
     * Get all glossary words which are relevant to the current page
     *
     * @access protected
     * @return void
     */
    protected function getGlossaryWords()
    {
        $pidList = array();
        if (!empty($this->config['pidList'])) {
            if (strtolower($this->config['pidList']) == 'pagetree') {
                if (count($GLOBALS['TSFE']->rootLine)) {
                    foreach ($GLOBALS['TSFE']->rootLine as $rootPage) {
                        $pidList[] = $rootPage['uid'];
                    }
                }
            } else {
                $pidList = GeneralUtility::intExplode(',', $this->config['pidList']);
            }
        }
        $andWhere = '';
        if (count($pidList)) {
            $andWhere = ' AND pid IN (' . join(',', $pidList) . ')';
        }
        // limit to the requested language
        $andWhere .= ' AND tx_gbglossary_domain_model_definition.sys_language_uid = ' . intval($GLOBALS['TSFE']->sys_language_uid);
        // respect enableFields
        $andWhere .= ' ' . $this->cObj->enableFields('tx_gbglossary_domain_model_definition');
        $result = $this->getDatabaseConnection()->exec_SELECTgetRows('*', 'tx_gbglossary_domain_model_definition', 'tx_gbglossary_domain_model_definition.exclude = 0 ' . $andWhere, '', '');
        if (is_array($result) && count($result)) {
            foreach ($result as $definition) {
                $index = ($this->config['ignoreCase'] == TRUE ? strtolower($definition['short']) : $definition['short']);
                $this->glossaryEntries[$index] = $definition;
                $this->glossaryIndex[$definition['uid']] = $index;
            }
        }
    }

    /**
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection;
     */
    protected function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * Parse HTML string, turn it into a DOMDocument and return the first body tag
     *
     * @param string $content
     */
    protected function parseHTMLContent($content)
    {
        $htmlCode = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=' . mb_detect_encoding($content) . '"></head><body>';
        $htmlCode .= preg_replace('/(^[[:space:]]+([\r\n]+))+/m', '', $content);
        $htmlCode .= '</body></html>';
        $domObject = $this->html5->loadHTML($htmlCode);
        $this->body = $domObject->getElementsByTagName('body')->item(0);
    }
}
