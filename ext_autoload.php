<?php
$extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('gb_glossary');
$extensionClassesPath = $extensionPath . 'Classes/';

return [
	'masterminds\html5' => $extensionPath . 'Libraries/HTML5.php',
	'masterminds\html5\parser\characterreference' => $extensionPath . 'Libraries/HTML5/Parser/CharacterReference.php',
	'masterminds\html5\parser\domtreebuilder' => $extensionPath . 'Libraries/HTML5/Parser/DOMTreeBuilder.php',
	'masterminds\html5\parser\eventhandler' => $extensionPath . 'Libraries/HTML5/Parser/EventHandler.php',
	'masterminds\html5\parser\fileinputstream' => $extensionPath . 'Libraries/HTML5/Parser/FileInputStream.php',
	'masterminds\html5\parser\inputstream' => $extensionPath . 'Libraries/HTML5/Parser/InputStream.php',
	'masterminds\html5\parser\parseerror' => $extensionPath . 'Libraries/HTML5/Parser/ParseError.php',
	'masterminds\html5\parser\scanner' => $extensionPath . 'Libraries/HTML5/Parser/Scanner.php',
	'masterminds\html5\parser\stringinputstream' => $extensionPath . 'Libraries/HTML5/Parser/StringInputStream.php',
	'masterminds\html5\parser\tokenizer' => $extensionPath . 'Libraries/HTML5/Parser/Tokenizer.php',
	'masterminds\html5\parser\treebuildingrules' => $extensionPath . 'Libraries/HTML5/Parser/TreeBuildingRules.php',
	'masterminds\html5\parser\utf8utils' => $extensionPath . 'Libraries/HTML5/Parser/UTF8Utils.php',
	'masterminds\html5\serializer\html5entities' => $extensionPath . 'Libraries/HTML5/Serializer/HTML5Entities.php',
	'masterminds\html5\serializer\outputrules' => $extensionPath . 'Libraries/HTML5/Serializer/OutputRules.php',
	'masterminds\html5\serializer\rulesinterface' => $extensionPath . 'Libraries/HTML5/Serializer/RulesInterface.php',
	'masterminds\html5\serializer\traverser' => $extensionPath . 'Libraries/HTML5/Serializer/Traverser.php',
	'masterminds\html5\elements' => $extensionPath . 'Libraries/HTML5/Elements.php',
	'masterminds\html5\entities' => $extensionPath . 'Libraries/HTML5/Entities.php',
	'masterminds\html5\exception' => $extensionPath . 'Libraries/HTML5/Exception.php',
	'masterminds\html5\instructionprocessor' => $extensionPath . 'Libraries/HTML5/InstructionProcessor.php',
];
